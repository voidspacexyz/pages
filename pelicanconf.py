#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

AUTHOR = u'voidspacexyz'
SITENAME = u'Exprimenting Gitlab Pages with Pelican'
SITEURL = ''

PATH = 'content'

TIMEZONE = 'Europe/Paris'

DEFAULT_LANG = u'en'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Blogroll
LINKS = (('Pelican', 'http://getpelican.com/'),
         ('Python.org', 'http://python.org/'),
         ('Jinja2', 'http://jinja.pocoo.org/'),
         ('You can modify those links in your config file', '#'),)

# Social widget
SOCIAL = (('You can add links in your config file', '#'),
          ('Another social link', '#'),)
THEME = 'theme/'
THEME_STATIC_DIR = 'static'
DEFAULT_PAGINATION = 10
USE_FOLDER_AS_CATEGORY = True

#OUTPUT_PATH = 'public/'
# Uncomment following line if you want document-relative URLs when developing
RELATIVE_URLS = True
